#include "led.h"

#include <Arduino.h>

namespace {

// Time when color routine must act next time
uint32_t colorNextToggleTime = 0;

// If led active and blinking bigger than 0, then led will be off. 
bool ledIsOn = false;

constexpr uint8_t color_red[] = {255, 0,   0};
constexpr uint8_t color_green[] = {0,   255, 0};
constexpr uint8_t color_bright_green[] = {153, 204, 0};
constexpr uint8_t color_blue[] = {0,   0,   255};
constexpr uint8_t color_bright_blue[] = {0,   204, 255};
constexpr uint8_t color_orange[] = {255, 102, 0};
constexpr uint8_t color_purple[] = {128, 0,   128};

struct LedColorTableEntry_t {
    Led::Led_Mode_e mode;
    const uint8_t* color;
    const char* name;
    uint8_t blinkingTimes;
};

#define LED_COLOR_ENTRY(a,b,c,d) a,b,c,d

constexpr LedColorTableEntry_t ledColorTable[] = {
    {LED_COLOR_ENTRY(Led::Led_Mode_e::ARMED_NO_GPS,   color_blue,         "ARMED_NO_GPS", 0) },
    {LED_COLOR_ENTRY(Led::Led_Mode_e::ARMED_GPS,      color_green,        "ARMED_GPS",    0) },
    {LED_COLOR_ENTRY(Led::Led_Mode_e::LOW_BATTERY,    color_orange,       "LOW_BATTERY",  0) },
    {LED_COLOR_ENTRY(Led::Led_Mode_e::READY_NO_GPS,   color_bright_blue,  "READY_NO_GPS", 2) },
    {LED_COLOR_ENTRY(Led::Led_Mode_e::READY_GPS,      color_bright_green, "READY_GPS",    2) },
    {LED_COLOR_ENTRY(Led::Led_Mode_e::ERROR,          color_red,          "ERROR",        4) }
};

const LedColorTableEntry_t* currentLedColorEntry = nullptr;
#define LED_COLOR_ENTRY_COUNT (sizeof(ledColorTable) / sizeof(LedColorTableEntry_t))

}

Led::Led(uint8_t red_pin, uint8_t green_pin, uint8_t blue_pin)
    : _red_pin(red_pin), _green_pin(green_pin), _blue_pin(blue_pin)
{
    pinMode(_red_pin,OUTPUT);
    pinMode(_green_pin,OUTPUT);
    pinMode(_blue_pin,OUTPUT);  
}

void Led::turn_on(uint8_t red_val, uint8_t green_val, uint8_t blue_val)
{
    analogWrite(_red_pin,red_val);
    analogWrite(_green_pin,green_val);
    analogWrite(_blue_pin,blue_val);
    ledIsOn = true;
}


void Led::turn_off() {
    analogWrite(_red_pin,0);
    analogWrite(_green_pin,0);
    analogWrite(_blue_pin,0);
    ledIsOn = false;
}

void Led::stop() {
    analogWrite(_red_pin,0);
    analogWrite(_green_pin,0);
    analogWrite(_blue_pin,0);
    ledIsOn = false;
    currentLedColorEntry = nullptr;
    colorNextToggleTime = 0;
}

void Led::color(uint8_t red_val,uint8_t green_val,uint8_t blue_val)
{
    turn_on(red_val, green_val, blue_val);
}


void Led::color(Led_Mode_e mode)
{
    const LedColorTableEntry_t* selectedCandidate = nullptr;
    for(uint8_t i = 0; i < LED_COLOR_ENTRY_COUNT; i++) {
        const LedColorTableEntry_t* candidate = &ledColorTable[i];
        if(candidate->mode != mode) {
            continue;
        }

        if(!currentLedColorEntry) {
            selectedCandidate = candidate;
            break;
        }
        selectedCandidate = candidate;
        break;
    }

    if(!selectedCandidate) {
        return;
    }

    currentLedColorEntry = selectedCandidate;
    colorNextToggleTime = 0;
}

// updateColor -> must be run at least 10Hz
void Led::colorUpdate(uint32_t currentTime)
{
    if(currentLedColorEntry == nullptr) {
        return;
    }

    if(currentLedColorEntry->blinkingTimes == 0) {
        turn_on(currentLedColorEntry->color[0],currentLedColorEntry->color[1],currentLedColorEntry->color[2]);
        return;
    }

    if(colorNextToggleTime > currentTime) {
        return;
    }

    if(!ledIsOn) {
        turn_on(currentLedColorEntry->color[0],currentLedColorEntry->color[1],currentLedColorEntry->color[2]);
    } else {
        turn_off();
    }

    colorNextToggleTime = currentTime + (uint32_t)(1000/currentLedColorEntry->blinkingTimes);
}
