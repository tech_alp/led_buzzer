#ifndef _LED_H
#define _LED_H

#define SLOW_BLINK        500  // half second
#define FAST_BLINK        150  // 4 times per second
#define SUPERFAST_BLINK   100  // 10 times per second 

#include <stdint.h>

class Led {
public:
    enum class Led_Mode_e {
        ARMED_NO_GPS = 0,  //blue
        ARMED_GPS,         //green
        LOW_BATTERY,       //orange
        READY_NO_GPS,      //blue with blinking
        READY_GPS,         //green with blinking
        ERROR              //red with blinking
    };

    enum class Led_Color_Pin_e {
        RED_PIN = 0,
        GREEN_PIN,
        BLUE_PIN,
        ALL
    };

    Led(uint8_t red_pin, uint8_t green_pin, uint8_t blue_pin);

    void turn_on(uint8_t red_val, uint8_t green_val, uint8_t blue_val);
    void turn_off();
    void stop();
    
    void color(uint8_t red,uint8_t green,uint8_t blue);
    void color(Led_Mode_e status);

    void colorUpdate(uint32_t currentTime);


private:
    uint8_t _red_pin;
    uint8_t _green_pin;
    uint8_t _blue_pin;
};

#endif