#define _TASK_PRIORITY 

#include <Arduino.h>
#include <DirectIO.h>
#include <TaskScheduler.h>

#include "led.h"
#include "buzzer.h"

Led led(3,5,6);
Buzzer buzz(9);

Scheduler ts,infoTS;

///**************************************************
void updateCB();
bool updateOE();
Task tUpdate(10,-1,&updateCB,&infoTS,true);

void updateCB() {
    uint32_t time = millis();
    buzz.beeperUpdate(time);
    led.colorUpdate(time);
}

bool updateOE() {
    Serial.println("update");
    buzz.beeper(Buzzer::BEEPER_SYSTEM_INIT);
    return true;
}
///**************************************************

///**************************************************
void serialEventCB();
Task tSerialEvent(100,-1,&serialEventCB,&ts,true);

void serialEventCB() {
    if(Serial.available()) {
        char ch = Serial.read();
        Serial.println(ch);
        if(ch == '1') {
            Serial.println("Error");
            led.color(Led::Led_Mode_e::ERROR);
        } else if(ch == '2') {
            Serial.println("ArmedNoGPS");
            buzz.beeper(Buzzer::BEEPER_ARMING_GPS_NO_FIX);
            led.color(Led::Led_Mode_e::ARMED_NO_GPS);
        } else if(ch == '3') {
            Serial.println("lowBattery");
            buzz.beeper(Buzzer::BEEPER_BAT_LOW);
            led.color(Led::Led_Mode_e::LOW_BATTERY);
        } else if(ch == '4') {
            Serial.println("armedGps");
            buzz.beeper(Buzzer::BEEPER_ARMING_GPS_FIX);
            led.color(Led::Led_Mode_e::ARMED_GPS);
        } else if(ch == '5') {
            Serial.println("readyGps");
            led.color(Led::Led_Mode_e::READY_GPS);
        buzz.beeper(Buzzer::BEEPER_SYSTEM_INIT);
        } else if(ch == '6') {
            Serial.println("SYSTEM_INIT");
            buzz.beeper(Buzzer::BEEPER_GYRO_CALIBRATED);
            led.color(Led::Led_Mode_e::READY_NO_GPS);
        }
    }
}
///**************************************************

///**************************************************
OutputPin outpin(13);
void init_system() {
    outpin = HIGH;
    buzz.beeper(Buzzer::BEEPER_SYSTEM_INIT);
}

void setup() {
    Serial.begin(9600);
    ts.setHighPriorityScheduler(&infoTS);
    delay(2000);
    init_system();
}

void loop() {
    ts.execute();
}

// byte servoPin = 7; // signal pin for the ESC.
// byte potentiometerPin = A0; // analog input pin for the potentiometer.
// Servo servo;

// void setup() {
// Serial.begin(9600);
// servo.attach(servoPin);
// servo.writeMicroseconds(1500); // send "stop" signal to ESC. Also necessary to arm the ESC.

// delay(7000); // delay to allow the ESC to recognize the stopped signal.
// }

// void loop() {

// int potVal = analogRead(potentiometerPin); // read input from potentiometer.

// int pwmVal = map(potVal,0, 1023, 1100, 1900); // maps potentiometer values to PWM value.

// Serial.print("pwmval = ");
// Serial.println(pwmVal);

// servo.writeMicroseconds(pwmVal); // Send signal to ESC.
// }


/*
Scheduler ts, ledTS;

void turn_off_all() {
    Serial.println("Disabled");
    led.turn_off();
}

void errorInfoLed();
bool errorInfoLedOE();
void armedNoGpsLed();
bool armedNoGpsLedOE();
bool lowBateryOE();
void readyGpsCB();
bool readyGpsOE();
bool armedGpsOE();

Task errorLedTask(FAST_BLINK,-1,&errorInfoLed,&ledTS,false);
Task armedNoGpsTask(TASK_HOUR,TASK_ONCE,NULL,&ledTS,false,&armedNoGpsLedOE);
Task lowBatteryTask(TASK_HOUR,TASK_ONCE,NULL,&ledTS,false,&lowBateryOE);
Task armedGpsTask(TASK_HOUR,TASK_ONCE,NULL,&ledTS,false,&armedGpsOE);
Task readyGpsTask(500,-1,&readyGpsCB,&ledTS,false);

//------------------------------------------------------
//errorLedTask
bool erroState = false;
void errorInfoLed() {
    if(erroState) {
        led.color(Led::Led_Mode_e::ERROR);
    } else {
        led.turn_off();
    }
    erroState = !erroState;
}

//------------------------------------------------------
//armedNoGpsTask

void armedNoGpsLed() {

}

bool armedNoGpsLedOE() {
    Serial.println("armedNoGpsLedOE");
    errorLedTask.disable();
    
    led.color(Led::Led_Mode_e::ARMED_NO_GPS);
    return true;
}

//------------------------------------------------------
//lowBatteryTask
bool lowBateryOE() {
    lowBatteryTask.enable();
    led.color(Led::Led_Mode_e::LOW_BATTERY);
    return true;
}

//------------------------------------------------------
//readyGpsTask
bool readyGpsState = false;
void readyGpsCB() {
    if(readyGpsState) {
        led.color(Led::Led_Mode_e::READY_GPS);
    } else {
        led.turn_off();
    }
    readyGpsState = !readyGpsState;
}

bool readyGpsOE() {
    led.color(Led::Led_Mode_e::READY_GPS);
    return true;
}

//------------------------------------------------------
//
bool armedGpsOE() {
    armedGpsTask.enable();
    led.color(Led::Led_Mode_e::ARMED_GPS);
    return true;
}

//------------------------------------------------------
//
void BatLowCB();
Task tbuzzBatLow(250,-1,&BatLowCB,&ledTS,false);
void BatLowCB() {
    buzz.beeper(Buzzer::BEEPER_BAT_LOW);
    buzz.beeperUpdate(millis());
}

//------------------------------------------------------
//
void disarmingCB();
Task tdisarming(250,-1,&disarmingCB,&ledTS,false);
void disarmingCB() {
    buzz.beeper(Buzzer::BEEPER_DISARMING);
    buzz.beeperUpdate(millis());
}


//------------------------------------------------------
void serial_event_();
Task serialManagerTask(100,-1,&serial_event_,&ts,true);

void serial_event_() {
    if(Serial.available()) {
        char ch = Serial.read();
        Serial.println(ch);
        if(ch == '1') {
            Serial.println("Error");
            ledTS.disableAll();
            errorLedTask.restartDelayed();
        } else if(ch == '2') {
            Serial.println("ArmedNoGPS");
            ledTS.disableAll();
            armedNoGpsTask.restartDelayed();
        } else if(ch == '3') {
            Serial.println("lowBattery");
            ledTS.disableAll();
            lowBatteryTask.restartDelayed();
            tbuzzBatLow.restartDelayed();
        } else if(ch == '4') {
            Serial.println("armedGps");
            ledTS.disableAll();
            armedGpsTask.restartDelayed();
        } else if(ch == '5') {
            Serial.println("readyGps");
            ledTS.disableAll();
            readyGpsTask.restartDelayed();
            tdisarming.restartDelayed();
        }
    }
}
*/